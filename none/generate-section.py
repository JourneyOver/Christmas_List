#!/usr/bin/env python

from sys import argv as argv

item = argv[1];
item_nows = argv[1].replace(" ","")
link = argv[2]

print('''<section>
    <label for="%(item_nows)s" class="item-heading">%(item)s</label>
    <div class="expanding-box">
        <a href="%(link)s">&lt;online link&gt;</a>
        <br>
        <p class="item-desc">details</p>
    </div>
</section>''' % locals())