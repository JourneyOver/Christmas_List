# **Computer Accessories**:

1. HyperX Alloy Elite RGB - Mechanical Gaming Keyboard - Cherry MX Brown
2. CORSAIR Scimitar Pro RGB
3. Elgato Stream Deck
4. Blackout Yeti USB microphone
5. UGREEN SD Card Reader

# **Other Electronics**:

1. CanaKit Raspberry Pi 4 4GB Starter Kit

# **Nintendo Switch Accessories**:

1. SanDisk 256GB MicroSDXC SD Card
2. Nintendo Switch Pro Controller
3. Carrying Storage Case for Nintendo Switch

# **Nintendo Switch Games**:

1. Luigi's Mansion 3
2. Mario Kart 8 Deluxe
3. Legend of Zelda Link's Awakening
4. New Super Mario Bros. U Deluxe
5. Super Mario Odyssey
6. A Hat In Time
7. Xenoblade Chronicles 2
8. Tales of Vesperia
9. Splatoon 2

# **Misc.**:

1. BOSSIN Gaming Chair

# **Clothing**:

1. Pajama Clothing
2. Shorts/Boxers
3. Shirts
4. Pants
